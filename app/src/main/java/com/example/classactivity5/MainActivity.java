package com.example.classactivity5;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.ListView;


public class MainActivity extends AppCompatActivity {

    int[] images = {R.drawable.peshawar_1, R.drawable.karachi_1, R.drawable.lahore_1, R.drawable.quetta_1, R.drawable.islamabd_1, R.drawable.multan_1};

    String[] version = {"Peshawar Zalmi", "Karachi Kings", "Lahore Qalanders", "Quetta Gladiators", "Islamabad United", "Multan Sultan"};

    ListView lView;

    ListAdapter lAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lView = (ListView) findViewById(R.id.List1);

        lAdapter = new ListAdapter(MainActivity.this, version, images);

        lView.setAdapter(lAdapter);



    }
}